function populateUFs(){
    const ufSelect = document.querySelector("select[name=uf]")
    fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados")
    .then(res => res.json())
    .then( states => {
        for(const state of states){
            ufSelect.innerHTML += `<option value="${state.id}">${state.nome}</option>`
        }
    })
}
populateUFs()

function getCities(event){  
    const citySelect = document.querySelector("select[name=city]")
    const stateInput = document.querySelector("input[name=state]")
    const ufValue = event.target.value
    
    const indexOfSelectedState = event.target.selectedIndex
    stateInput.value = event.target.options[indexOfSelectedState].text
    
    const url = `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${ufValue}/municipios`
   
    // essas duas linhasabaixo é pra limpar o cache do campo de cidades
    // e poder atualizar com os valores certos das cidades correspondendo o estado.
    citySelect.innerHTML = "<option value>Selecione a Cidade</option>"
    citySelect.disabled  = true

    fetch(url)
    .then( res => res.json() )
    .then( cities => {
        
        for(const city of cities){
            citySelect.innerHTML += `<option value="${city.nome}">${city.nome}</option>`
            
        }
        citySelect.disabled  = false
    })
}
 
 document
    .querySelector("select[name=uf]")
    .addEventListener("change", getCities) //esse addEventListener escuta o evento para começas as ações.
    

    //Itens de Coleta
// Pegar todos os li
const itemsToCollect = document.querySelectorAll(".items-grid li")
for (const item of itemsToCollect){
    item.addEventListener("click", handleSelectedItem)
}

const collectedTtem = document.querySelector("input[name=items]")

let selectedItems = [] //let é uma variavel q pode mudar de valor depois, o const n
function handleSelectedItem(event){
    const itemLi = event.target 

    //add ou remover uma classe com javascript
    itemLi.classList.toggle("selected")

    const itemId = event.target.dataset.id

    //verificar se existe itens selecionados, se sim
    //pegar os itens selecionados
    const alreadySelected = selectedItems.findIndex(item => {
        const itemFound =  item == itemId //== igualando um número com o outro e será vdd ou falso
        return itemFound
    })

    //se já estiver selecionado,
    if(alreadySelected >= 0){

        //remover da seleção
        const filteredItems = selectedItems.filter(item =>{
            const itemIsDifferent = item != itemId
            return itemIsDifferent
        })

        selectedItems = filteredItems

    }else{ //se não estiver selecioonado, adicionar a seleção, adicionar a seleção
        selectedItems.push(itemId)
    }
    
    //atualizar o campo escondido com os itens selecionados
   collectedTtem.value = selectedItems
}